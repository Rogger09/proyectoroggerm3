package proyectoM3Rogger;

import java.util.Scanner;

public class combates {
	static Scanner reader = new Scanner(System.in);

	// Stats Personaje
	public static String nombre;
	public static int jugador_hp = 200;
	public static int jugador_def = 100;
	public static int jugador_atq = 20;

	public static int jugador_lvl = 1;
	public static int jugador_exp = 0;

	public static int jugador_hp_base = 200;
	public static int jugador_def_base = 100;
	public static int jugador_atq_base = 10;

	// Stats Enemigos
	public static int enemigo_numero = 1;
	public static int enemigo_hp = 40;
	public static int enemigo_def = 10;
	public static int enemigo_atq = 20;

	public static int enemigo_hp_base = 40;
	public static int enemigo_def_base = 5;
	public static int enemigo_atq_base = 20;

	// Stats Bosses
	public static int boss_numero = 1;
	public static int boss_hp = 100;
	public static int boss_def = 50;
	public static int boss_atq = 40;

	public static int boss_hp_base = 100;
	public static int boss_def_base = 50;
	public static int boss_atq_base = 40;

	// Vidas
	// public static int jugador_vidas = 3;

	// Pisos
	public static int pisos = 1;

	// Items
	public static int poti_hp_peque�a = 50;
	public static int poti_hp_grande = 100;
	public static int poti_def_peque�a = 25;
	public static int poti_def_grande = 50;

	// Inventario
	public static int stack_hp_peque�a = 0;
	public static int stack_hp_grande = 0;
	public static int stack_def_peque�a = 0;
	public static int stack_def_grande = 0;

	public static void main(String[] args) {

		System.out.println("*****************************************************************************\r\n"
				+ "*****************************************************************************\r\n"
				+ "************------------------------------------------------------***********\r\n"
				+ "************|����������������������������������������������������|***********\r\n"
				+ "************|����������������������������������������������������|***********\r\n"
				+ "************|����������������������������������������������������|***********\r\n"
				+ "************|���	____ ____ _ _ _ _ _ _ ____ ____       ���|***********\r\n"
				+ "************|���	|__/ |  | | | | | | | |___ |__/       ���|***********\r\n"
				+ "************|���	|  \\ |__| |_|_| |_|_| |___ |  \\       ���|***********\r\n"
				+ "************|���       	      ____ ___  ____         	      ���|***********\r\n"
				+ "************|���     	       |__/ |__] | __         	      ���|***********\r\n"
				+ "************|���               |  \\ |    |__]         	      ���|***********\r\n"
				+ "************|���					      ���|***********\r\n"
				+ "************|����������������������������������������������������|***********\r\n"
				+ "************|����������������������������������������������������|***********\r\n"
				+ "************|����������������������������������������������������|***********\r\n"
				+ "************------------------------------------------------------***********\r\n"
				+ "*****************************************************************************\r\n"
				+ "*****************************************************************************");

		int opcio;
		do {
			opcio = menu();
			switch (opcio) {
			case 0:
				break;
			case 1:
				inicio();
				opcio = menuInicio();
				break;
			default:
				System.out.println("No puedes hacer esto...");
				break;
			}
		} while (opcio != 0);
	}

	public static int menu() {
		int opcio;
		System.out.println("1. Iniciar Partida");
		System.out.println("0. Salir");
		do {
			System.out.println("Di una opci�n:");
			opcio = reader.nextInt();
			if (opcio > 1 || opcio < 0)
				System.out.println("Introduce una opci�n correcta");
		} while (opcio > 1 || opcio < 0);
		return opcio;
	}

	public static void inicio() {
		System.out.println("Dinos tu nombre: ");
		reader.nextLine();
		nombre = reader.nextLine();
		System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
		System.out.println("Te pondre en situaci�n:");
		System.out.println("Eres un heroe invocado en un mundo muy diferente al mundo que conocias, en el cual hay");
		System.out.println("una enorme y misterosa torre de exactamente 100 pisos en los cuales cada piso tiene");
		System.out.println("9 enemigos y un jefe de piso, mientras mas pisos subas los enemigos se hacen mas");
		System.out.println("fuertes... pero no solo ellos ya que tu al derrotarlos ganas Experiencia con la que");
		System.out.println("subes de nivel y aumentar tus estadisticas, los enemigos al ser derrotados pueden");
		System.out.println("llegar a soltar algunos items que te serviran en las batallas y los jefes de piso");
		System.out.println("si o si te daran un item.");
		System.out.println("Al ser un heroe invocado no has aparecido desnudo... si no que has sido invocado con");
		System.out.println("una espada y una armadura digna de un heroe.");
		System.out.println("Dicho esto, que empiece tu aventura...");
		System.out.println("\n\n");
	}

	public static boolean menuJugador() {
		int opcio;
		
		if (pisos == 101) {
			bossFinal();
		}
		
		statsJugador();
		statsEnemigo();
		do {
			System.out.println("Menu Jugador");
			System.out.println("1. Seguir adelante	2. Ver inventario");
			System.out.println("3. Ver estadisticas	0. Salir del juego");
			System.out.println("Piso Actual: " + pisos);
			opcio = reader.nextInt();
			switch (opcio) {
			case 0:
				System.exit(0);
				break;
			case 1:
				return true;
			case 2:
				inventario();
				break;
			case 3:
				statsJugador();
				break;
			default:
				System.out.println("No puedes hacer esto...");
				break;
			}
		} while (opcio != 0);
		return false;
	}

	public static int menuInicio() {
		int opcio;
		do {
			System.out.println("Menu Usuario");
			System.out.println("1. Seguir adelante	0. Salir del Juego");
			opcio = reader.nextInt();
			switch (opcio) {
			case 0:
				System.exit(0);
				break;
			case 1:
				avanzar();
				break;
			default:
				System.out.println("No puedes hacer esto...");
				break;
			}
		} while (opcio != 0);
		return opcio;

	}

	public static void statsJugador() {
		System.out.println("Las estadisticas actuales de: " + nombre);
		System.out.println("Vida:	" + jugador_hp + "	Defensa: " + jugador_def + "	Ataque: " + jugador_atq);
		System.out.println("Nivel:	" + jugador_lvl + "	Experiencia: " + jugador_exp + "/100");
		System.out.println("\n");
	}

	public static void statsEnemigo() {
		System.out.println("Estadisticas del siguiente enemigo n�mero " + enemigo_numero);
		System.out.println("Vida:	" + enemigo_hp + "	Defensa: " + enemigo_def + "	Ataque: " + enemigo_atq);
		System.out.println("\n");
	}

	public static void avanzar() {
		boolean resultado_jugador;
		boolean resultado_enemigo = false;
		boolean resultado_jefe = false;

		int rondas = 9; // Numero total de batallas contra Enemigos
		reader.nextLine();
		do {

			for (int i = 0; i < rondas; i++) {
				System.out.println(nombre + " encuentra al Enemigo N�: " + enemigo_numero);
				do {
					System.out.println("\n");
					System.out.println("Las Estadisticas del Enemigo actual son: ");
					System.out.println(
							"Vida:	" + enemigo_hp + "	Defensa: " + enemigo_def + "	Ataque: " + enemigo_atq);
					System.out.println("\n");
					System.out.println("Tus Estadisticas actuales son: ");
					System.out.println(
							"Vida:	" + jugador_hp + "	Defensa: " + jugador_def + "	Ataque: " + jugador_atq);
					System.out.println("\n");

					resultado_jugador = turnoJugador();
					if (resultado_jugador == false) {
						resultado_enemigo = turnoEnemigo();
					}
					System.out.println("Enter para seguir...");
					reader.nextLine();
					System.out.println("----------------------------------------------------------------------------");
				} while (resultado_jugador == false && resultado_enemigo == false);

				if (resultado_enemigo == true) {
					int opcio;
					do {
						System.out.println("GAME OVER");
						System.out.println("\n");
						System.out.println("Quieres volver a intentarlo?");
						System.out.println("1. SI	0.NO");
						opcio = reader.nextInt();
						switch (opcio) {
						case 0:
							System.exit(0);
							break;
						case 1:
							reiniciar();
							menuInicio();
							break;
						default:
							System.out.println("No puedes hacer esto...");
							break;
						}
					} while (opcio != 0);
				} else {
					if (menuJugador() == false) {
						return;
					}
				}
			}

			// Lucha Boss
			System.out.println("\n");

			reader.nextLine();

			do {
				System.out.println("\n");
				System.out.println("Tus Estadisticas actuales son: ");
				System.out
						.println("Vida:	" + jugador_hp + "	Defensa: " + jugador_def + "	Ataque: " + jugador_atq);
				System.out.println("\n");
				System.out.println("Las Estadisticas del Boss actual son: ");
				System.out.println("Vida:	" + boss_hp + "	Defensa: " + boss_def + "	Ataque: " + boss_atq);
				System.out.println("\n");

				menuLuchaBoss();

				// El jugador ataca
				resultado_jugador = turnoJugadorVSBoss();

				// Si no mata al boss
				if (resultado_jugador == false) {
					// El boss ataca al jugador
					resultado_enemigo = turnoBoss();
				}

			} while (resultado_jugador == false && resultado_enemigo == false);

		} while (pisos <= 100);

		bossFinal();
	}

	public static void inventario() {
		int opcio;
		do {
			System.out.println("Inventario de " + nombre);
			System.out.println("Pociones de Vida");
			System.out.println("Peque�as: " + stack_hp_peque�a + "	Grandes: " + stack_hp_grande);
			System.out.println("\n");
			System.out.println("Pociones de Defensa");
			System.out.println("Peque�as: " + stack_def_peque�a + "	Grandes: " + stack_def_grande);
			System.out.println("\n");
			System.out.println("1. Usar poci�n");
			System.out.println("2. Salir del inventario");
			opcio = reader.nextInt();
			switch (opcio) {
			case 1:
				usarPotis();
				break;
			}
		} while (opcio != 2);
	}

	public static int menuBatallaBoss() {
		int opcio;
		System.out.println("1. Atacar");
		System.out.println("2. Defenderse");
		System.out.println("2. Usar Items");
		do {
			opcio = reader.nextInt();
			if (opcio > 3 || opcio < 0)
				System.out.println("Introduce una opci�n correcta");
		} while (opcio > 3 || opcio < 0);
		return opcio;
	}

	public static void pausa(int segons) {

		try {
			Thread.sleep(segons * 1000);
		} catch (Exception e) {

		}
	}

	public static boolean turnoJugador() { // Turno Jugador
		int da�oJugador; // Da�o inflingido por el jugador

		da�oJugador = jugador_atq + (int) (Math.random() * 3) - 2; // Calcular da�o del jugador entre su da�o maximo
																	// menos 3 aleatoriamente

		if (enemigo_def > 0) { // Si el enemigo tiene Defensa (Escudo)
			enemigo_def = enemigo_def - da�oJugador;
			System.out.println(nombre + " ha infringido: " + da�oJugador + " puntos de Da�o.");
			if (enemigo_def < 0) { // Si el enemigo no tiene escudo
				enemigo_def = Math.abs(enemigo_def); // Volver da�o negativo en positivo
				enemigo_hp = enemigo_hp - enemigo_def;
				enemigo_def = 0;
			}
		} else {
			enemigo_hp = enemigo_hp - da�oJugador;
			System.out.println(nombre + " ha infringido: " + da�oJugador + " puntos de Da�o.");
		}
		if (enemigo_hp <= 0) { // Vida de enemigo a 0 o menos {
			System.out.println("Felicidades, el enemigo ha muerto.");
			enemigo_numero++;
			pisos++;
			expJugador();
			dropsEnemigos();
			mejorarEnemigo();
			return true;
		}
		return false;
	}

	public static boolean turnoEnemigo() { // Turno Enemigo
		int da�oEnemigo; // Da�o inflingido por el Enemigo

		da�oEnemigo = enemigo_atq + (int) (Math.random() * 3) - 2; // Calcular da�o del enemigo entre su da�o maximo
																	// menos 3 aleatoriamente

		System.out.println("El enemigo ha infringido: " + da�oEnemigo + " puntos de Da�o.");

		if (jugador_def > 0) { // Da�o enemigo a nuestra defensa
			jugador_def = jugador_def - da�oEnemigo;
			if (jugador_def < 0) { // Si el jugador no tiene escudo
				jugador_def = Math.abs(jugador_def);
				jugador_hp = jugador_hp - jugador_def;
				jugador_def = 0;
			}
		} else {
			jugador_hp = jugador_hp - da�oEnemigo; // Da�o enemigo si el jugador no tiene defensa
		}
		if (jugador_hp <= 0) { // Vida del jugador a 0 (Muerte)
			System.out.println("\n");
			System.out.println(nombre + " ha muerto...");
			return true;
			// jugador_vidas--; // Restar vidas al jugador
			// if (jugador_vidas == 0) { // Si el jugador se queda sin vidas
			// }
		}
		return false;
	}

	public static boolean turnoJugadorVSBoss() { // Turno Jugador Contra un Boss
		int da�oJugador; // Da�o inflingido por el jugador

		da�oJugador = jugador_atq + (int) (Math.random() * 3) - 2; // Calcular da�o del jugador entre su da�o maximo
																	// menos 3 aleatoriamente

		if (boss_def > 0) { // Si el boss tiene Defensa (Escudo)
			boss_def = boss_def - da�oJugador;
			System.out.println(nombre + " ha infringido: " + da�oJugador + " puntos de Da�o.");
			if (boss_def < 0) { // Si el boss no tiene escudo
				boss_def = Math.abs(boss_def); // Volver da�o negativo en positivo
				boss_hp = boss_hp - boss_def;
				boss_def = 0;
			}

		} else {
			boss_hp = boss_hp - da�oJugador;
			System.out.println(nombre + " ha infringido: " + da�oJugador + " puntos de Da�o.");
		}
		if (boss_hp <= 0) { // Vida de Boss a 0 o menos {
			System.out.println("Felicidades, has derrotado al Boss!");
			enemigo_numero++;
			pisos++;
			expJugador();
			dropsBoss();
			mejorarBoss();
			menuJugador();
			return true;
		}
		return false;
	}

	public static boolean turnoBoss() {
		int da�oBoss; // Da�o inflingido por el Boss

		da�oBoss = boss_atq + (int) (Math.random() * 3) - 2; // Calcular da�o del enemigo entre su da�o maximo menos 3
																// aleatoriamente

		System.out.println("El Boss ha infringido: " + da�oBoss + " puntos de Da�o.");

		if (jugador_def > 0) { // Da�o Boss a nuestra defensa
			jugador_def = jugador_def - da�oBoss;
			if (jugador_def < 0) { // Si el jugador no tiene escudo
				jugador_def = Math.abs(jugador_def);
				jugador_hp = jugador_hp - jugador_def;
				jugador_def = 0;
			}
		} else {
			jugador_hp = jugador_hp - da�oBoss; // Da�o Boss si el jugador no tiene defensa
		}
		if (jugador_hp <= 0) { // Vida del jugador a 0 (Muerte)
			System.out.println("\n");
			System.out.println(nombre + " ha muerto...");
			// jugador_vidas--; // Restar vidas al jugador
			// if (jugador_vidas == 0) { // Si el jugador se queda sin vidas
			// }
			return true;
		}
		return false;
	}

	public static void menuLuchaBoss() {
		int opcio;

		do {
			System.out.println("Menu Batalla VS Boss");
			System.out.println("1. Atacar		2. Abrir Inventario");
			System.out.println("0. Salir del juego");
			System.out.println("\n");
			opcio = reader.nextInt();
			switch (opcio) {
			case 0:
				System.exit(0);
				break;
			case 1:
				return;
			case 2:
				inventario();
				break;
			default:
				System.out.println("No puedes hacer esto...");
				break;
			}
		} while (opcio != 0);
	}

	public static void dropsEnemigos() {
		int suerte;
		suerte = (int) (Math.random() * 3 + 1); // N�mero random entre 1,2 y 3

		if (suerte == 1) {
			stack_hp_peque�a = stack_hp_peque�a + 1; // A�adimos una poti peque�a de Vida
			System.out.println("Has obtenido una poci�n de Vida Peque�a");
			System.out.println("\n");
		}

		if (suerte == 2) {
			stack_def_peque�a = stack_def_peque�a + 1; // A�adimos una poti peque�a de Defensa
			System.out.println("Has obtenido una poci�n de Defensa Peque�a");
			System.out.println("\n");
		}

		if (suerte == 3) {
			System.out.println("El enemigo no a soltado nada....");
			System.out.println("\n");
		}
	}

	public static void dropsBoss() {
		int suerte;
		suerte = (int) (Math.random() * 3 + 1); // N�mero random entre 1,2 y 3

		if (suerte == 1) {
			stack_hp_grande = stack_hp_grande + 2; // A�adimos una poti peque�a de Vida
			System.out.println("Has obtenido 2 pociones de Vida Peque�a");
			System.out.println("\n");
		}

		if (suerte == 2) {
			stack_def_grande = stack_def_grande + 2; // A�adimos una poti peque�a de Defensa
			System.out.println("Has obtenido dos pociones de Defensa Peque�a");
			System.out.println("\n");
		}

		if (suerte == 3) {
			stack_hp_grande = stack_hp_grande + 2;
			stack_def_grande = stack_def_grande + 2;
			System.out.println("FELICIDADES has obtenido 2 pociones de de Vida y Defensa!");
			System.out.println("\n");
		}
	}

	public static void mejorarEnemigo() {
		int stats_hp_enemigo = 0;
		int stats_atq_enemigo = 0;
		int stats_def_enemigo = 0;
		double multiplicador;

		// Reiniciamos los stats del enemigo ya que si no su vida sera negativa
		enemigo_hp = enemigo_hp_base;
		enemigo_def = enemigo_def_base;
		enemigo_atq = enemigo_atq_base;

		// Vida enemigo
		if (pisos >= 75)
			multiplicador = 0.5;
		else if (pisos >= 50)
			multiplicador = 0.10;
		else if (pisos >= 25)
			multiplicador = 0.15;
		else
			multiplicador = 0.20;
		stats_hp_enemigo = (int) (enemigo_hp_base * multiplicador); // Hacemos una operaciun de porcentaje y la
																	// guardamos en la
		// nueva variable
		enemigo_hp_base = enemigo_hp_base + stats_hp_enemigo; // A�adimos el resultado a la variable del jugador
		enemigo_hp = enemigo_hp + stats_hp_enemigo;

		// Ataque enemigo
		if (pisos >= 75)
			multiplicador = 0.5;
		else if (pisos >= 50)
			multiplicador = 0.10;
		else if (pisos >= 25)
			multiplicador = 0.15;
		else
			multiplicador = 0.20;
		stats_atq_enemigo = (int) (enemigo_atq_base * multiplicador);
		enemigo_atq_base = enemigo_atq_base + stats_atq_enemigo;
		enemigo_atq = enemigo_atq + stats_atq_enemigo;

		// Def enemigo
		if (pisos >= 75)
			multiplicador = 0.5;
		else if (pisos >= 50)
			multiplicador = 0.10;
		else if (pisos >= 25)
			multiplicador = 0.15;
		else
			multiplicador = 0.20;
		stats_def_enemigo = (int) (enemigo_def_base * multiplicador);
		enemigo_def_base = enemigo_def_base + stats_def_enemigo;
		enemigo_def = enemigo_def + stats_def_enemigo;

		System.out.println("\n");
		System.out.println("Al avanzar de piso los enemigos se hacen mas fuertes...");
		System.out.println(
				"HP + " + stats_hp_enemigo + "		Atq + " + stats_atq_enemigo + "	Def + " + stats_def_enemigo);
		System.out.println("\n");

		stats_atq_enemigo = 0; // Reiniciamos el resultado de la variable
		stats_hp_enemigo = 0;
		stats_def_enemigo = 0;

		// for (int i = 0; i < 40; i++) {
		// enemigo_da�o_base = (int) (enemigo_da�o_base * 1.05);
		// System.out.println(enemigo_da�o_base);
		// }
	}

	public static void mejorarBoss() {
		int stats_hp_boss = 0;
		int stats_atq_boss = 0;
		int stats_def_boss = 0;

		// Reiniciamos los stats del Boss ya que si no su vida sera negativa
		boss_hp = boss_hp_base;
		boss_def = boss_def_base;
		boss_atq = boss_atq_base;

		// Vida Boss
		stats_hp_boss = (int) (boss_hp_base * 1.30); // Hacemos una operaciun de porcentaje y la guardamos en la
														// nueva variable
		boss_hp_base = boss_hp_base + stats_hp_boss; // A�adimos el resultado a la variable del jugador
		boss_hp = boss_hp + stats_hp_boss;

		// Ataque Boss
		stats_atq_boss = (int) (boss_atq_base * 1.05);
		boss_atq_base = boss_atq_base + stats_atq_boss;
		boss_atq = boss_atq + stats_atq_boss;

		// Def Boss
		stats_def_boss = (int) (enemigo_def_base * 1.20);
		boss_def_base = boss_def_base + stats_def_boss;
		boss_def = boss_def + stats_def_boss;

		System.out.println("\n");
		System.out.println("El siguiente boss no ser� tan f�cil...");
		System.out.println("HP + " + stats_hp_boss + "		Atq + " + stats_atq_boss + "	Def + " + stats_def_boss);
		System.out.println("\n");

		stats_atq_boss = 0; // Reiniciamos el resultado de la variable
		stats_hp_boss = 0;
		stats_def_boss = 0;

		// for (int i = 0; i < 40; i++) {
		// boss_da�o_base = (int) (boss_da�o_base * 1.05);
		// System.out.println(boss_da�o_base);
		// }
	}

	public static void usarPotis() {
		int opcio;
		System.out.println("\n");
		System.out.println("Pociones:");
		if (stack_hp_peque�a == 0 && stack_hp_grande == 0 && stack_def_peque�a == 0 && stack_def_grande == 0) {
			System.out.println("Actualmente no tienes ninguna pocion.");
		}
		
		if (stack_hp_peque�a != 0) {
			System.out.println("1. Vida Peque�a (50 HP): " + stack_hp_peque�a);
		}
		if (stack_hp_grande != 0) {
			System.out.println("2. Vida Grande (100 HP): " + stack_hp_grande);
		}
		
		if (stack_def_peque�a != 0) {
			System.out.println("3. Defensa Peque�a (25 HP): " + stack_def_peque�a);
		}
		
		if (stack_def_grande != 0) {
			System.out.println("4. Defensa Grande (50 DEF): " + stack_def_grande);
		}
		System.out.println("5. Regresar");
		System.out.println("\n");
		do {
			opcio = reader.nextInt();
			switch (opcio) {
			case 1:
				if (stack_hp_peque�a > 0) {
					System.out.println("Has usado una poci�n de Vida Peque�a");
					jugador_hp = jugador_hp + poti_hp_peque�a;
					stack_hp_peque�a--;
				} else {
					System.out.println("Actualmente no tienes ninguna poci�n de Vida Peque�a.");
				}
				break;
			case 2:
				if (stack_hp_grande > 0) {
					System.out.println("Has usado una poci�n de Vida Grande");
					jugador_hp = jugador_hp + poti_hp_grande;
					stack_hp_grande--;
				} else {
					System.out.println("Actualmente no tienes ninguna poci�n de Vida Grande.");
				}
				break;
			case 3:
				if (stack_def_peque�a > 0) {
					System.out.println("Has usado una poci�n de Defensa Peque�a");
					jugador_def = jugador_def + poti_def_peque�a;
					stack_def_peque�a--;
				} else {
					System.out.println("Actualmente no tienes ninguna poci�n de Defensa Peque�a.");
				}
				break;
			case 4:
				if (stack_def_grande > 0) {
					System.out.println("Has usado una poci�n de Defensa Grande");
					jugador_def = jugador_def + poti_def_grande;
					stack_def_grande--;
				} else {
					System.out.println("Actualmente no tienes ninguna poci�n de Defensa Grande.");
				}
				break;
			}
		} while (opcio != 5);
	}

	public static void reiniciar() {
		// Jugador
		jugador_hp = 200;
		jugador_def = 100;
		jugador_atq = 20;

		// LVL EXP
		jugador_lvl = 1;
		jugador_exp = 0;

		// Jugador Base
		jugador_hp_base = 200;
		jugador_def_base = 100;
		jugador_atq_base = 10;

		// Enemigo
		enemigo_numero = 1;
		enemigo_hp = 40;
		enemigo_def = 10;
		enemigo_atq = 20;

		// Enemigo Base
		enemigo_hp_base = 40;
		enemigo_def_base = 5;
		enemigo_atq_base = 20;

		// Boss
		boss_numero = 1;
		boss_hp = 100;
		boss_def = 50;
		boss_atq = 40;

		// Boss Base
		boss_hp_base = 100;
		boss_def_base = 50;
		boss_atq_base = 40;

		// Piso
		pisos = 1;

		// Stack Potis (Inventario)
		stack_hp_peque�a = 0;
		stack_hp_grande = 0;
		stack_def_peque�a = 0;
		stack_def_grande = 0;

		// Vidas
		// jugador_vidas = 3;
	}

	public static void bossFinal() {
		int opcio;
		System.out.println("Wow... has logrado derrotar a todos los enemigos y completar los 100 pisos de esta tore..");
		System.out.println("De verdad eres el Heroe de la leyenda.");
		System.out.println("\n");

		do {
			System.out.println("Quieres volver a empezar?");
			System.out.println("1. SI	0.NO");
			opcio = reader.nextInt();
			switch (opcio) {
			case 0:
				System.exit(0);
				break;
			case 1:
				reiniciar();
				menuInicio();
				break;
			default:
				System.out.println("No puedes hacer esto...");
				break;
			}
		} while (opcio != 0);
	}

	public static void expJugador() {

		int stats_hp = 0; // variable para guardar la operaci�n vida
		int stats_atq = 0; // variable para guardar la operaci�n ataque

		jugador_exp = jugador_exp + 100; // Experiencia del jugador + 50

		if (jugador_exp == 100) { // Si la experiencia del jugador es 100
			if (pisos <= 10) {
				stats_hp = 10;
				stats_atq = 8;
			}
			if (pisos > 10 && pisos <= 20) {
				stats_hp = 15;
				stats_atq = 14;
			}
			if (pisos > 20 && pisos <= 30) {
				stats_hp = 22;
				stats_atq = 18;
			}
			if (pisos > 30 && pisos <= 40) {
				stats_hp = 28;
				stats_atq = 26;
			}
			if (pisos > 40 && pisos <= 50) {
				stats_hp = 36;
				stats_atq = 32;
			}
			if (pisos > 50 && pisos <= 60) {
				stats_hp = 46;
				stats_atq = 34;
			}
			if (pisos > 60 && pisos <= 70) {
				stats_hp = 56;
				stats_atq = 44;
			}
			if (pisos > 70 && pisos <= 80) {
				stats_hp = 62;
				stats_atq = 50;
			}
			if (pisos > 80 && pisos <= 90) {
				stats_hp = 70;
				stats_atq = 58;
			}
			if (pisos > 90 && pisos <= 99) {
				stats_hp = 78;
				stats_atq = 64;
			}
			
//			stats_hp = (int) (jugador_hp_base * 0.60); // Operacion del da�o base del jugador multiplicado por un % y
//														// guardarla en una variable.
//			stats_atq = (int) (jugador_atq * 0.10); // Operacion de la vida base del jugador multiplicado por un % y
//													// guardarla en una variable.
			jugador_lvl++; // Subir de nivel al jugador
			jugador_hp = jugador_hp + stats_hp; // A�adir a la vida del jugador los datos de la operacion (stats_hp)
			jugador_atq = jugador_atq + stats_atq; // A�adir al ataque del jugador los datos de la operacion (stats_atq)
			System.out.println("\n");
			System.out.println("Felicidades acabas de subir de Nivel, tus Estadisticas acaban de subir...");
			System.out.println("Tu Vida acaba de subir: " + stats_hp + " puntos.");
			System.out.println("Tu Ataque acaba de subir: " + stats_atq + " puntos.");
			System.out.println("\n");
			jugador_hp_base = jugador_hp_base + stats_hp;
			stats_hp = 0; // reiniciar la variable donde guardamos la operacion de la vida
			stats_atq = 0; // reiniciar la variable donde guardamos la operacion del ataque
			jugador_exp = 0; // reiniciar la experiencia a 0
			statsJugador();
		}
	}
}
